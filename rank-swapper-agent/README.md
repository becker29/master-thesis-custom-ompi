# OpenMPI Rank Swapper Agent

0. Set the required environment variable `OMPI` which is most probably the path to your directory `openmpi-install` where the current  OpenMPI-installation resides (just replace `<path-to-my-openmpi-installation>`). You can export it either in the `Makefile` (if a real MPI program should be run) or in the `client.sh` (if the test client should be run) or in your current shell if you do not mind repeating that for different shell instances.

1. Compile everything:
```shell
make all
```

2. Starting the Agent/Server:
```shell
./server.sh
```

3. Starting the OpenMPI-Client (other MPI-Programs should also suffice):
```shell
./client.sh
```

