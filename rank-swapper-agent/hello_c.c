/*
 * Copyright (c) 2004-2006 The Trustees of Indiana University and Indiana
 *                         University Research and Technology
 *                         Corporation.  All rights reserved.
 * Copyright (c) 2006      Cisco Systems, Inc.  All rights reserved.
 *
 * Sample MPI "hello world" application in C
 */

#include <stdio.h>
#include <stdlib.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <unistd.h>

#include "mpi.h"

#define BUFFLEN 64

extern char **environ;

/* Spawn Modes (dynamic job spawning):
 *   1: Spawn just one process (in one job)
 *   2: Spawn 2 processes in 2 different jobs
 *   3: Spawn 3 processes in one (shared) job
 *   0 or other: Do not spawn a dynamic process/job
 */
#define SPAWN_MODE 1

int main(int argc, char *argv[])
{
    FILE *ptr = fopen("/home/test_out", "a");
    fprintf(ptr, "TEST APP STARTED\n");
    char **s = environ;

    // PRINT ENV
    for (; *s; s++) {
        fprintf(ptr, "%s\n", *s);
    }

    char *parentPort = getenv("OMPI_PARENT_PORT");
    if (parentPort) {
        fprintf(ptr, "Hey we have a parent port! %s", parentPort);
    }
    fclose(ptr);

    int rank, size, len;
    pid_t pid;
    char version[MPI_MAX_LIBRARY_VERSION_STRING];

    MPI_Init(&argc, &argv);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    MPI_Comm_size(MPI_COMM_WORLD, &size);

    pid = getpid();

    // notifyProcessAgent(pid, rank, "Spawned");

    printf("Hello, world, I am %d of %d, PID: %d\n", rank, size, pid);
    fflush(stdout);

    // dynamically spawn child process
    // https://mpi.deino.net/mpi_functions/MPI_Comm_spawn.html
    if (1 == SPAWN_MODE) {

        int np = 2;
        int errcodes[2];
        MPI_Comm parentcomm, intercomm;
        MPI_Comm_get_parent(&parentcomm);
        if (parentcomm == MPI_COMM_NULL) {
            MPI_Comm_spawn("/home/ompi/rank-swapper-agent/hello", MPI_ARGV_NULL, np, MPI_INFO_NULL,
                           0, MPI_COMM_WORLD, &intercomm, errcodes);
            printf("I'm the parent.\n");
            fflush(stdout);
        } else {
            printf("I'm the spawned.\n");
            fflush(stdout);
        }
        if (0 != errcodes[0]) {
            printf("ERROR_SPAWN: code: %d\n", errcodes[0]);
            fflush(stdout);
        }

        // (instead of Comm_multiple) spawns a second, different intercommunicator
    } else if (2 == SPAWN_MODE) {

        int np = 2;
        int errcodes[2];
        MPI_Comm parentcomm;
        MPI_Comm intercomm[2];
        MPI_Comm_get_parent(&parentcomm);
        if (parentcomm == MPI_COMM_NULL) {
            for (int i = 0; i < 2; i++) {
                MPI_Comm_spawn("/home/ompi/rank-swapper-agent/hello", MPI_ARGV_NULL, np,
                               MPI_INFO_NULL, 0, MPI_COMM_WORLD, &intercomm[i], errcodes);
                if (0 != errcodes[0]) {
                    printf("ERROR_SPAWN: code: %d\n", errcodes[0]);
                    fflush(stdout);
                }
            }
            printf("I'm the parent.\n");
            fflush(stdout);
        } else {
            printf("I'm the spawned.\n");
            fflush(stdout);
        }

    } else if (3 == SPAWN_MODE) {

        int np[2] = {2, 1};
        int errcodes[3];
        MPI_Comm parentcomm, intercomm;
        char *cmds[3] = {"/home/ompi/rank-swapper-agent/hello",
                         "/home/ompi/rank-swapper-agent/hello",
                         "/home/ompi/rank-swapper-agent/hello"};
        MPI_Info infos[3] = {MPI_INFO_NULL, MPI_INFO_NULL, MPI_INFO_NULL};
        MPI_Comm_get_parent(&parentcomm);
        if (parentcomm == MPI_COMM_NULL) {
            // Create n more processes using the "hello" executable
            MPI_Comm_spawn_multiple(2, cmds, MPI_ARGVS_NULL, np, infos, 0, MPI_COMM_WORLD,
                                    &intercomm, errcodes);
            printf("I'm the parent.\n");
            fflush(stdout);
        } else {
            printf("I'm the spawned.\n");
            fflush(stdout);
        }
        for (int i = 0; i < 3; i++) {
            if (0 != errcodes[i]) {
                printf("ERROR_SPAWN: code: %d\n", errcodes[i]);
                fflush(stdout);
            }
        }
    }

    // printf("Sleeping.\n");
    // fflush(stdout);
    // sleep(5);
    // printf("Done sleeping.\n");
    // fflush(stdout);

    MPI_Finalize();

    // notifyProcessAgent(pid, rank, "Ended");

    return 0;
}
