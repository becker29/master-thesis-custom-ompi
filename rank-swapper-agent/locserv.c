/// locserv
//
// Plain TCP-server process (for testing purposes), which:
// * accepts a connection to a MPI-client-process
// * prints the incoming process-info-data (ranks, jobids, proc-counts)
//   from the client to std:out
// * answers the client with a list of modified ranks

#include <stdarg.h>
#include <stdio.h>
#include <stdint.h>
#include <string.h>
#include <stdlib.h>
#include <netdb.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <sys/un.h>
#include <time.h>
#include <unistd.h>
#include <endian.h>

#define MAX_CONNECTIONS 10
#define BUFFLEN  256

void errorExit(char* msg);

int main(void) {

    // TCP server based on http://www.microhowto.info/howto/listen_for_and_accept_tcp_connections_in_c.html

    char client_message[BUFFLEN];

    const char* host_ip = "localhost";

    const char * agent_port = getenv("DPM_AGENT_PORT");
    if (NULL == agent_port) {
        errorExit("Could not find DPM_AGENT_PORT env");
    }

    struct addrinfo hints;
    memset(&hints, 0, sizeof(hints));
    hints.ai_family = AF_UNSPEC;
    hints.ai_socktype = SOCK_STREAM;
    hints.ai_protocol = 0;
    hints.ai_flags = AI_PASSIVE|AI_ADDRCONFIG;
    struct addrinfo* result = 0;
    int error_code = getaddrinfo(host_ip, agent_port, &hints, &result);
    if (0 != error_code) {
        errorExit("Could not get addrinfo!");
    }

    int server_fd = socket(result->ai_family,result->ai_socktype,result->ai_protocol);
    if (-1 == server_fd) {
        errorExit("Could not create server socket!");
    }

    int reuseaddr = 1;
    if (-1 == setsockopt(server_fd, SOL_SOCKET, SO_REUSEADDR, &reuseaddr, sizeof(reuseaddr))) {
        errorExit("Could not set SO_REUSEADDR!");
    }

    if (-1 == bind(server_fd, result->ai_addr, result->ai_addrlen)) {
        errorExit("Could not bind address!");
    }

    freeaddrinfo(result);

    if (0 != listen(server_fd, MAX_CONNECTIONS)) {
        errorExit("failed to listen for connections");
    }
    
    int session_fd = 0;
    while ((session_fd = accept(server_fd, NULL, NULL)) >= 0) {
    
        // read the length and discard it (by overwriting it next)
        recv(session_fd, client_message, sizeof(uint32_t), 0);
        printf("Length-Msg: %u\n", le32toh(*(uint32_t *)client_message));
        // since recv() seems to be atomar, no concurrent behaviour must be
        // considered
        recv(session_fd, client_message, BUFFLEN, 0);
        printf("Message: %s\n", client_message);

        pid_t pid = 0;
        uint32_t vpid = 0;
        uint32_t jobid = 0;
        size_t size = 0;
        int vars_read = sscanf(client_message, "{\"msg_type\": 128, \"msg_data\": \"%d,%u,%u,%zu\"}", &pid, &vpid, &jobid, &size);
        if (4 != vars_read) {
            errorExit("Message could not be parsed: Too many/few entries in msg_data!");
        }
        printf("Sscanf read count: %d (should equal 4)\n", vars_read);
        printf("Spawned - PID: %d, vpid: %u, jobID: %u, size: %zu\n", pid, vpid, jobid, size);
        
        // generate a comma separated list of ascending integers to be used as ranks
        char rankList[BUFFLEN / 2] = "";
        char first_rank[] = "0";
        strcat(rankList, first_rank);
        for (uint32_t i = 1; i < size; i++) {
            char rank[BUFFLEN];
            sprintf(rank, ",%u", i);
            strcat(rankList, rank);
        }
        char end[] = "\0";
        strcat(rankList, end);

        char rankString[BUFFLEN];
        snprintf(rankString, BUFFLEN, "{\"msg_type\": 0, \"msg_data\": \"%s\"}", rankList);
        // the length must be send first to emulate the behaviour of the actual TCP server but it
        // gets discarded on client side anyway so it does not need to make sense
        uint32_t needed_length = strlen(rankString) + 1;
        uint32_t le_needed_length = le32toh(needed_length);
        send(session_fd, &le_needed_length, sizeof(uint32_t), 0);
        // send the actual message
        if (send(session_fd, rankString, needed_length, 0) < 0) {
                errorExit("send");
        }
        printf("Send Rank: %s\n\n", rankString);
        
        close(session_fd);
    }
    
    close(server_fd);
    
    return 0;
}

